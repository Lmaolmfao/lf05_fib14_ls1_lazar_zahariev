import java.util.Scanner;
public class Weg {
	static Scanner eingabe = new Scanner(System.in);
	public static void main(String[] args) {
		int fahrtzeit = 0;
		String ziel = "";
		System.out.println("zul�ssige Eingaben \"j\", \"ja\", \"n\", \"nein\"");
		if (abfragen("Fahrt nach Hamburg?") == false) {
			fahrtzeit = fahrtzeit + 34;
			if (abfragen("halt in Stendal?") == true) {
				fahrtzeit = fahrtzeit + 16;
			} else {
				fahrtzeit = fahrtzeit + 6;
			}
			if (abfragen("endet in Wolfsburg?") == true) {
				fahrtzeit = fahrtzeit + 29;
				ziel = "Wolfsburg";
			} else if (abfragen("endet in Hannover?") == true) {
				fahrtzeit = fahrtzeit + 63;
				ziel = "Hannover";
			} else if (abfragen("endet in Braunschweig?") == true) {
				fahrtzeit = fahrtzeit + 50;
				ziel = "Braunschweig";
			}
			System.out.println("Die Fahrtzeit nach " + ziel + " betr�gt " + fahrtzeit + " Minuten");
		} else {
			System.out.println("Die Fahrtzeit nach Hamburg �ber Spandau dauert 96 Minuten");
		}
	}
	private static boolean abfragen(String pText) {
		System.out.println(pText);
		switch(eingabe.next()) {
		case "ja": return true;
		case "j": return true;
		case "nein": return false;
		case "n": return false;
		default: System.out.println("unzul�ssige Eingabe, automatisch \"nein\""); return false;
		}
	}

}
