public class Konfigurationstest {
	
	public static void main(String[]args) 
	{
		System.out.println("1) Aufgaben zu Variablen, output:");
		machenAufgabe1();
		System.out.println("2) Aufgabe zu Operatoren, output:");
		machenAufgabe2();
	}  
	
	public static void machenAufgabe1() 
	{
		//Aufgabe 1.1
		int cent;
		cent = 80;
		System.out.println(cent);
		cent = 90;
		System.out.println(cent);
		double maximum = 95.50;
		
		//Aufgabe 1.2
		boolean wahr = true;           
		int minus = -1000;         
		double kommazahl = 1.255;  
		char hashtag = '#';
		
		//Aufgabe 1.3
		String satz = "Hier ein Satz";
		final int check_nr = 8765;
		
		//Aufgabe 1.4
		/*Weil alle Operationen auf der Manipulation von Datentypen aufgebaut sind.
		 * Verschiedene Datentypen verbrauchen nur soviel Speicher wie es braucht um die
		 *  m�glichen Werte dar zu stellen.                                                   */
	}
	
	public static void machenAufgabe2() 
	{
		//Aufgabe 2.1
		int ergebnis;
		ergebnis = 4+8*9-1;
		System.out.println(ergebnis);
		
		//Aufgabe 2.2
		int zaehler = 1;
		int max = 10;
		for (zaehler = 1;zaehler<=max;zaehler++) {
			System.out.print(zaehler+ " ");
		}
		System.out.println();
		
		//Aufgabe 2.3
		int divis;
		divis = 22/6;
		System.out.println(divis);
		
		//Aufgabe 2.4
		int schalter = 10;
		boolean vergleich1 = ((schalter > 7)&&(schalter < 12));
		System.out.println(vergleich1);
		
		//Aufgabe 2.5
		boolean vergleich2 = ((schalter != 7)||(schalter == 12));
		System.out.println(vergleich2);
	}
}
