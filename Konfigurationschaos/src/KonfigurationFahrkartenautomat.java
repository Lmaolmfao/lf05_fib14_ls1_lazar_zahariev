public class KonfigurationFahrkartenautomat 
{
	public static void main(String[]args) 
	{
		String typ = "Automat AVR";
		String bezeichnung = "Q2021_FAB_A";
		String name = typ + " " + bezeichnung;
		char sprachModul = 'd';
		final byte PRUEFNR = 4;
		double patrone = 46.24;
		double maximum = 100.00;
		double prozent = maximum - patrone;
		short muenzenCent = 1280;
		short muenzenEuro = 130;
		int summe = muenzenCent + muenzenEuro * 100;
		int cent = summe % 100;
		int euro = summe / 100;
		boolean status = ((euro <= 150)&&(euro >= 50)&&(cent != 0)&&(sprachModul == 'd'));
		System.out.println("Name: " + name);
		System.out.println("Sprache: " + sprachModul);
		System.out.println("Pr�fnummer : " + PRUEFNR);
		System.out.println("F�llstand Patrone: " + prozent + " %");
		System.out.println("Summe Euro: " + euro +  " Euro");
		System.out.println("Summe Rest: " + cent +  " Cent");		
		System.out.println("Status: " + status);
	} 
}
