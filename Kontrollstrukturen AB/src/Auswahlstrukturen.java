public class Auswahlstrukturen 
{
	public static void main(String[] args) 
	{
		aufgabe1(3, 2);
		aufgabe2(1,3,2);
	}
	private static void aufgabe1(int a, int b) 
	{
		if (a == b) 
		{
			ausgeben(a + " und " + b + " sind gleich.");
		}
		if (a > b) 
		{
			ausgeben(b + " ist kleiner als " + a);
		}
		if (a < b) 
		{
			ausgeben(a + " ist kleiner als" + b);
		}
	}
	private static void aufgabe2(int a, int b, int c) {
		if ((a > b)&&(b == c)) {
			ausgeben(a+" ist groesser als "+ b +" aber "+ b +" = " + c);
		}
		else if ((c > a)||(c > b)) {
			ausgeben(c + " ist groesser als " + a + " oder " + b);
		}
		int max = a;
		int zahlen[] = {a,b,c};
		for (int i = 0; i<3;i++) {
			if (max < zahlen[i]) {
				max = zahlen[i];
			}
		}
		//test
		ausgeben("die groesste Zahl ist " + max);
	}
	private static void ausgeben(String pText) {
		System.out.println(pText);
	}
}
