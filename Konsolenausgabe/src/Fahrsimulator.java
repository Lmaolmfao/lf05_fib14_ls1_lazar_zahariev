//AB-MethodenProgrammierübungen Aufgabe 6
import java.util.Scanner;
public class Fahrsimulator {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		double v = 0;
		
		//eine Schleife die mit der Eingabe 0 beendet wird zur Simulation
		//der Beschleunigung
		boolean runtime = true;
		while(runtime == true) 
		{
			double wert = tastatur.nextDouble();
			if (wert == 0) {
				runtime = false;
				System.out.println("Ablauf beendet.");
			}
			else 
			{
				v = beschleunige(v, wert);
				System.out.printf("%.1f\n", v);
			}
		}
	}
	private static double beschleunige(double pV, double pDv) {
		double newV = pV+pDv;
		return newV;
	}
}
