//AB-MethodenProgrammierübungen Aufgabe 2
import java.util.Scanner;
public class PcHaendler {
	static Scanner myScanner = new Scanner(System.in);
	public static void main(String[] args) {
		// Benutzereingaben lesen
		String artikel = readArtikel("was möchten Sie bestellen?");
		int anzahl = readAnz("Geben Sie die Anzahl ein:");
		double preis = readDouble("Geben Sie den Nettopreis ein:");
		double mwst = readDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");

		// Verarbeiten
		double nettogesamtpreis = nettogesamtpreis(anzahl, preis);
		double bruttogesamtpreis = bruttogesamtpreis(nettogesamtpreis, mwst);

		// Ausgeben
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
	public static String readArtikel(String pText) {
		System.out.println(pText);
		String lString = myScanner.next();
		return lString;
	}
	public static int readAnz(String pText) {
		System.out.println(pText);
		int lInt = myScanner.nextInt();
		return lInt;
	}
	public static double readDouble(String pText) {
		System.out.println(pText);
		double lDouble = myScanner.nextDouble();
		return lDouble;
	}
	public static double nettogesamtpreis(int pAnzahl, double pPreis) {
		return pAnzahl * pPreis;
	}
	public static double bruttogesamtpreis(double pNetto, double pMwst) {
		return pNetto * (1 + pMwst / 100);
	}
}
