//AB-MethodenProgrammierübungen Aufgabe 5
public class Mathe {
	public static void main(String[] args) {
		System.out.println(hypothenuse(10,5));
	}
	public static double quadrat(double pWert) {
		return pWert*pWert;
	}
//AB-MethodenProgrammierübungen Aufgabe 7
	public static double hypothenuse(double pKat1, double pKat2) {
		return Math.sqrt(quadrat(pKat1)+quadrat(pKat2));
	}
}
