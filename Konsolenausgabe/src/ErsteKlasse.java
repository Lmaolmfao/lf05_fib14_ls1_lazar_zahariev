public class ErsteKlasse {

	public static void main(String[] args) {
		//Ausgabe zur Funktionskontrolle
		System.out.println("Aufgabe 1: " +mittelwert(20,100));
		System.out.println("Aufgabe 3: " + reihenschaltung(200, 1000));
		System.out.printf("Aufgabe 4: %.2f", parallelschaltung(20, 100));
		

	}
	//AB-MethodenProgrammierübungen Aufgabe 1
 	private static double mittelwert(double pErste, double pZweite) {
 		double m = (pErste+pZweite)/2;
 		return m;
 	}
 	//AB-MethodenProgrammierübungen Aufgabe 3
 	private static double reihenschaltung(double r1, double r2) {
 		return r1 + r2;
 	}
 	//AB-MethodenProgrammierübungen Aufgabe 4
 	private static double parallelschaltung(double r1, double r2) {
 		return  1/r1+1/r2;
 	}
}
