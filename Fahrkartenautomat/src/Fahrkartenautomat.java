import java.util.Scanner;
class Fahrkartenautomat
{
	static String[] karten = {"Kurzstrecke Berlin", "Einzelfahrschein Regeltarif AB", "Tageskarte Regeltarif AB", "Kleingruppen-Tageskarte Regeltarif AB"};
	static double[] preise = {2, 2.90, 8.60, 23.50};
	static int resetZeit = 4000; //Zeit nach welcher der Automat bereit ist eine weitere Bestellung anzunehemn
	static Scanner tastatur = new Scanner(System.in);
    public static void main(String[] args)
    {
    	boolean start = true;
    	while (start == true) {
    	//Leerzeilen f�r Optik
    	for (int i = 0; i < 10; i++) {
    		System.out.println();
    	}
    	displayAngebote();
    	double zuZahlenderWert = fahrkartenbestellungErfassen();
    	double bezahlterWert = fahrkartenBezahlen(zuZahlenderWert);
    	fahrkartenAusgeben();
    	rueckgeldAusgeben(zuZahlenderWert, bezahlterWert);
    	try {
  			Thread.sleep(resetZeit);
  		} catch (InterruptedException e) {
  			e.printStackTrace();
  			}
    	}
      
    }
    private static void displayAngebote() {
    	for (int i = 0; i < karten.length; i++) {
    	System.out.printf("%s [%.2f] (%d) \n",karten[i], preise[i],i);
    	}
    }
    private static double getPreis() {
    	boolean fehler = true;
    	double preis = 0;
    	while (fehler == true) 
    	{
        	System.out.printf("\nIhre Wahl: ");
    		int eingabe = tastatur.nextInt();
    		if ((eingabe >= 0)&&(eingabe <= karten.length)) {
        		fehler = false;
        		preis = preise[eingabe];
    		} else { 
    		System.out.println(" ++ung�ltige Eingabe++"); 
    		}
    	}
    	return preis;
    }
    private static int getAnzahl() {
    	boolean fehler = true;
    	int anzahl = 0;
    	while (fehler == true) 
    	{
        	System.out.print("Geben sie eine Anzahl zwischen 1 und 10 an: ");
        	try {
        		anzahl = tastatur.nextInt();
        		if ((anzahl > 0)&&(anzahl <= 10)) {
        			fehler = false;
        		} else {
        			System.out.println(" ++ung�ltige Eingabe++");
            		System.out.println();
        		}
        	} catch (Exception a) {
        		System.out.println(" ++ung�ltige Eingabe++");
        		System.out.println();
        		tastatur.nextLine();
        	}  
    	}
    	return anzahl;
    }
    private static boolean abfragen(String pText) {
		boolean fertig = false;
		boolean antwort = false;
		while (fertig != true) {
			System.out.println(pText);
			switch(tastatur.next()) {
			case "ja": antwort = true; fertig = true; break;
			case "j": antwort = true; fertig = true; break;
			case "nein": antwort = false; fertig = true; break;
			case "n": antwort = false; fertig = true; break;
			default: System.out.println(" ++ung�ltige Eingabe++");
			}
	    }
		return antwort;
	}
    private static double fahrkartenbestellungErfassen() {
    	double zuZahlenderBetrag;
    	double zwischensumme = 0;
    	boolean fertig = false;
    	while (fertig != true) {
    		zuZahlenderBetrag = getPreis();
    		int anzahlTickets = getAnzahl();
    		zwischensumme = zuZahlenderBetrag * anzahlTickets + zwischensumme;
    		System.out.printf(">> Zwischensumme: %.2f� \n\n", zwischensumme);
    		if (abfragen("wollen sie ein weiteres Ticket?") != true) {
    			fertig = true;
    		}
    	}
    	zuZahlenderBetrag = zwischensumme;
        return zuZahlenderBetrag;
    }
    private static double fahrkartenBezahlen(double pZuZahlen) {
    	double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < pZuZahlen)
        {
     	   double meinBetrag = pZuZahlen - eingezahlterGesamtbetrag;
     	   String format;
     	   //Passt die Formatierung auf die Zahl an
     	   if (meinBetrag-Math.round(meinBetrag) != 0) {
     		   format = "%.2f";
     	   } else {
     		   format = "%.0f";
     	   }
     	   System.out.printf("Noch zu zahlen: " + format , meinBetrag);
     	   System.out.print("�");
     	   System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   double eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
        return eingezahlterGesamtbetrag;
    }
    private static void fahrkartenAusgeben() {
    	 System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            try {
  			Thread.sleep(250);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         }
         System.out.println("\n\n");
    }
    private static void rueckgeldAusgeben(double pZuZahlen, double pBezahlt) {
    	double r�ckgabebetrag = pBezahlt - pZuZahlen;
         if(r�ckgabebetrag > 0.0)
         {
      	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", r�ckgabebetrag);
      	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

             while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
             {
          	  System.out.println("2 EURO");
  	          r�ckgabebetrag -= 2.0;
             }
             while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
             {
          	  System.out.println("1 EURO");
  	          r�ckgabebetrag -= 1.0;
             }
             while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
             {
          	  System.out.println("50 CENT");
  	          r�ckgabebetrag -= 0.5;
             }
             while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
             {
          	  System.out.println("20 CENT");
   	          r�ckgabebetrag -= 0.2;
             }
             while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
             {
          	  System.out.println("10 CENT");
  	          r�ckgabebetrag -= 0.1;
             }
             while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
             {
          	  System.out.println("5 CENT");
   	          r�ckgabebetrag -= 0.05;
             }
         }

         System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                            "vor Fahrtantritt entwerten zu lassen!\n"+
                            "Wir w�nschen Ihnen eine gute Fahrt.");
    }
}
