public class Sparbuch 
{
	private int kontonummer;
	private double kapital;
	private double zinssatz; 

	Sparbuch(int pKontonr, double pKapital, double pZins) {
		this.kontonummer = pKontonr;
		this.kapital = pKapital;
		this.zinssatz = pZins;
	}
	public void zahlEin(double pBetrag) {
		this.kapital = this.kapital + pBetrag;
	}
	public void hebeAb(double pBetrag) {
		if (this.kapital - pBetrag > 0.00) {
			this.kapital = this.kapital - pBetrag;
		}
		else {
			System.out.println("Kontostand zu niedrig!");
		}
	}
	public double getErtrag(int pLaufzeit) {
		double lErtrag = this.kapital;
		for (int i = 0; i<pLaufzeit; i++) {
			lErtrag = lErtrag*(1+(this.zinssatz/100));
		}
		return lErtrag;
	}
	public void verzinse() {
		this.kapital = getErtrag(1);
	}
	public int getKontonummer() {
		return this.kontonummer;
	}
	public double getKapital() {
		return this.kapital;
	}
	public double getZinssatz() {
		return this.zinssatz;
	}
}
